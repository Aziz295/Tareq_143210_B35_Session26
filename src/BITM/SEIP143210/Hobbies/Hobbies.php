<?php
namespace App\Hobbies;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class Hobbies extends DB{


    public $id;
    public $name="user";
    public $hobbies="";

    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data=NULL){

    if(array_key_exists('id',$data)){
        $this->id =$data['id'];
    }
    if(array_key_exists('name',$data)){


        $this->name= $data['name'];

    }
    if(array_key_exists('hobbies',$data)){

        $this->hobbies = $data['hobbies'];

    }

}
    public function store(){
        $str= implode(',',$this->hobbies);
        $arrData = array($this->name,$str);
        $sql = "INSERT INTO hobbies(name,hobbies) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::message("Data has been inserted successfully! :)");
        else
            Message::message("Your Data does not inserted. :(");

        Utility::redirect('create.php');

    }

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from hobbies');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();
    public function view($fetchMode='ASSOC'){
        $sql= 'SELECT * from hobbies where id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();
    public function update(){

        // $arrData = array($this->book_title, $this->author_name);

        $sql= "Update hobbies SET name = ?, hobbies[] = ? WHERE id=?";




        $STH = $this->DBH->prepare($sql);
        $STH->bindParam(1,$this->name);
        $STH->bindParam(2,$this->hobbies);
        $STH->bindParam(3,$this->id);

        $STH->execute();

        Utility::redirect('index.php');

    }//end of update();
    public function delete(){

        $sql = "Delete from hobbies WHERE id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');

    }
    public function trash(){
        $sql= "Update hobbies SET is_deleted =NOW() WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');
    }

}