<?php
namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $profile_picture;


    public function __construct()
    {

        parent::__construct();
    }


    public function setData($data = NULL)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('profile_picture', $data)) {
            $this->profile_picture = $data['profile_picture']['name'];
        }


    }
    /* public function setImageData($data=NULL)
     {
         if (array_key_exists('image', $data)) {
             $this->image = $data['image']['name'];
         }
     }*/

    public function store()
    {
        $path='C:\xampp\htdocs\Lab_Exam8_143210_B35\src\ProfilePicture\Picture';
        $uploadedFile = $path.basename($this->profile_picture);
        move_uploaded_file($_FILES['profile_picture']['tmp_name'], $uploadedFile);
        $arrData = array($this->name, $this->profile_picture);



        $sql = "insert into profile_picture(name, profile_picture) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql); //create a object
        $result = $STH->execute($arrData);
        if ($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }


    public function index($fetchMode='ASSOC'){
        $sql= "SELECT * from profile_picture WHERE is_deleted=0";


        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function view($fetchMode='ASSOC'){

        $sql='SELECT * from profile_picture WHERE id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();

    public function update(){
        $path='C:\xampp\htdocs\Lab_Exam8_143210_B35\src\ProfilePicture\Picture';
        if(!empty($_FILES['profile_picture']['name'])) {
            $uploadedFile = $path.basename($this->profile_picture);
            move_uploaded_file($_FILES['profile_picture']['tmp_name'],$uploadedFile);
            $arrData = array( $this->name,$this->profile_picture);
            $sql="UPDATE profile_picture SET name = ?, profile_picture = ? WHERE id=".$this->id;}
        else{
            $arrData = array( $this->name);
            $sql="UPDATE profile_picture SET name = ? WHERE id=".$this->id;
        }

        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    }// end of update method;
    public function delete(){
        $sql= "DELETE FROM profile_picture WHERE id =". $this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');

    }
    public function trash(){


        $sql= "UPDATE profile_picture SET is_deleted= NOW() WHERE id =". $this->id;


        $STH=$this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');


    }


}