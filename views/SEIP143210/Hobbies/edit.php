<?php
require_once("../../../vendor/autoload.php");
require_once("../../../menu.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo "<p id='message'>".Message::getMessage()."</p>";


use App\Hobbies\Hobbies;
$objHobbies=new Hobbies();

$objHobbies->setData($_GET);// set id in get method;



$oneData=$objHobbies->view('obj');






?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hobbies - Update Form</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../../resource/assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../resource/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../resource/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../resource/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../../resource/assets/ico/apple-touch-icon-57-precomposed.png">

</head>

<body>

<!-- Top content -->
<div class="top-content">


    <div class="container">


        <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Update Hobbies</h3>
                    <p>Update Your Name And Your Hobbies :</p>
                </div>
                <div class="form-top-right">
                    <i class="fa fa-book"></i>
                </div>
            </div>
            <div class="form-bottom">
                <form role="form" action="update.php" method="post" class="login-form">
                    <input type="hidden" name="id" value="<?php echo $oneData->id?>">

                    <div class="form-group">
                        <label class="sr-only" for="form-name">Name</label>

                        <input type="text" name="name" value="<?php echo $oneData->name?>" class="form-password form-control" id="form-password">
                    </div>
                    <div >
                        <table >
                            <tr>
                                <td colspan="2">Select Hobbies:</td>
                            </tr>
                            <tr>
                                <td>Walking</td>
                                <td><input type="checkbox" name="hobbies[]" value="walking" <?php if (in_array("walking",explode(',',$oneData->hobbies))) {echo "checked";}?> </td>
                            </tr>
                            <tr>
                                <td>Swimming</td>
                                <td><input type="checkbox" name="hobbies[]" value="swimming" <?php if (in_array("swimming",explode(',',$oneData->hobbies))) {echo "checked";}?>></td>
                            </tr>
                            <tr>
                                <td>Gardening</td>
                                <td><input type="checkbox" name="hobbies[]" value="gardening" <?php if (in_array("gardening",explode(',',$oneData->hobbies))) {echo "checked";}?>></td>
                            </tr>
                            <tr>
                                <td>Watching TV</td>
                                <td><input type="checkbox" name="hobbies[]" value="watchingtv" <?php if (in_array("watchingtv",explode(',',$oneData->hobbies))) {echo "checked";}?>></td>
                            </tr>
                            <tr>
                                <td>Others</td>
                                <td><input type="checkbox" name="hobbies[]" value="others" <?php if (in_array("others",explode(',',$oneData->hobbies))) {echo "checked";}?>></td>
                            </tr>

                        </table>
                    </div>
                    <button type="submit" class="btn">Update</button>
                </form>
            </div>
        </div>


    </div>
</div>

</div>


<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/assets/js/scripts.js"></script>

<!--script for message fadeout-->
<script>
    jQuery(document).ready(function($){
        $('#message').fadeOut(5555);


    });

</script>

</body>

</html>