<?php
require_once("../../../vendor/autoload.php");
use App\Hobbies\Hobbies;
$objHobbies = new Hobbies();

$objHobbies->setData($_GET);


$oneData=$objHobbies->view("obj");

echo "ID: ".$oneData->id."<br>";
echo "Name: ".$oneData->name."<br>";
echo "Hobbies: <br>";
$array= explode(",",$oneData->hobbies);
foreach($array as $a){
    echo $a."<br>";
}
